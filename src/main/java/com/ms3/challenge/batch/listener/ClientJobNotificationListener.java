package com.ms3.challenge.batch.listener;

import com.ms3.challenge.data.repository.ClientRepository;
import com.ms3.challenge.util.StatisticsHandlerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author earl
 */
@Component
public class ClientJobNotificationListener extends JobExecutionListenerSupport {

    private static final Logger log = LoggerFactory.getLogger(ClientJobNotificationListener.class);

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private StatisticsHandlerUtil statisticsHandlerUtil;

    @Override
    public void afterJob(JobExecution jobExecution) {
        if(jobExecution.getStatus() == BatchStatus.COMPLETED) {
            log.info("!!! Client JOB FINISHED! Time to verify the results");

            clientRepository.findAll().forEach(clientModel -> {
                log.info("found: "+clientModel.getClientName());
            });

            statisticsHandlerUtil.writeClientBadRecords();
            statisticsHandlerUtil.writeClientStatistics();
        }
    }
}
