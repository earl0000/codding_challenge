package com.ms3.challenge.batch.listener;

import com.ms3.challenge.util.StatisticsHandlerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemReadListener;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author earl
 */
public class TransactionItemReadListener implements ItemReadListener {

    private static final Logger log = LoggerFactory.getLogger(TransactionItemReadListener.class);


    @Autowired
    StatisticsHandlerUtil statisticsHandlerUtil;

    @Override
    public void beforeRead() {

    }

    @Override
    public void afterRead(Object item) {

    }

    @Override
    public void onReadError(Exception ex) {

        if(FlatFileParseException.class.isInstance(ex)){
            log.info("Bad file: " + ((FlatFileParseException) ex).getInput());
            statisticsHandlerUtil.getBadTransactionRecords().add(((FlatFileParseException) ex).getInput());
            statisticsHandlerUtil.increment(StatisticsHandlerUtil.TRANSACTIONFAILED);
            statisticsHandlerUtil.increment(StatisticsHandlerUtil.TRANSACTIONRECEIVED);
        }


    }
}
