package com.ms3.challenge.batch.writer;


import com.ms3.challenge.data.model.ClientModel;
import com.ms3.challenge.data.model.TransactionModel;
import com.ms3.challenge.data.repository.ClientRepository;
import com.ms3.challenge.data.repository.TransactionRepository;
import com.ms3.challenge.util.StatisticsHandlerUtil;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author earl
 */
@Service
public class TransactionWriter implements ItemWriter<TransactionModel> {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private StatisticsHandlerUtil statisticsHandlerUtil;

    @Override
    public void write(List<? extends TransactionModel> items) throws Exception {
        transactionRepository.saveAll(items);
        statisticsHandlerUtil.increment(StatisticsHandlerUtil.TRANSACTIONSUCCESS,items.size());
    }
}
