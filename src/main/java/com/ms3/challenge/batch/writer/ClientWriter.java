package com.ms3.challenge.batch.writer;


import com.ms3.challenge.data.model.ClientModel;
import com.ms3.challenge.data.repository.ClientRepository;
import com.ms3.challenge.util.StatisticsHandlerUtil;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author earl
 */
@Service
public class ClientWriter implements ItemWriter<ClientModel> {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private StatisticsHandlerUtil statisticsHandlerUtil;

    @Override
    public void write(List<? extends ClientModel> items) throws Exception {
        clientRepository.saveAll(items);
        statisticsHandlerUtil.increment(StatisticsHandlerUtil.CLIENTSUCCESS,items.size());
    }
}
