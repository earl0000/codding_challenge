package com.ms3.challenge.batch.processer;

import com.ms3.challenge.batch.business.Client;
import com.ms3.challenge.data.model.ClientModel;
import com.ms3.challenge.util.DateUtil;
import com.ms3.challenge.util.StatisticsHandlerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author earl
 */
public class ClientItemProcessor implements ItemProcessor<Client, ClientModel> {
    private static final Logger log = LoggerFactory.getLogger(ClientItemProcessor.class);


    @Autowired
    StatisticsHandlerUtil statisticsHandlerUtil;

    @Override
    public ClientModel process(Client item) throws Exception {
        log.info("---------------------Processing Client "+ item.getClientName());
        if (!item.getClientName().equals("Client Name")){
            statisticsHandlerUtil.increment(StatisticsHandlerUtil.CLIENTRECEIVED);
            ClientModel clientModel = new ClientModel();
            clientModel.setClientName(item.getClientName());
            clientModel.setContactNum(item.getContactNum());
            clientModel.setMailingAdd(item.getMailingAdd());
            clientModel.setBranch(item.getBranch());
            clientModel.setMemberSince(DateUtil.toLocalDate(item.getMemberSince()));
            return clientModel;
        }
        return null;
    }
}
