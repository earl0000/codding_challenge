package com.ms3.challenge.batch.processer;

import com.ms3.challenge.batch.business.Transaction;
import com.ms3.challenge.data.model.ClientModel;
import com.ms3.challenge.data.model.TransactionModel;
import com.ms3.challenge.data.repository.ClientRepository;
import com.ms3.challenge.util.DateUtil;
import com.ms3.challenge.util.StatisticsHandlerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

/**
 * @author earl
 */
public class TransactionItemProcessor implements ItemProcessor<Transaction, TransactionModel> {
    private static final Logger log = LoggerFactory.getLogger(TransactionItemProcessor.class);


    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private StatisticsHandlerUtil statisticsHandlerUtil;


    @Override
    public TransactionModel process(Transaction item) throws Exception {
        log.info("---------------------Processing Transaction for: "+ item.getClientName());
        try
        {
            if (!item.getClientName().equals("Client Name")){
                statisticsHandlerUtil.increment(StatisticsHandlerUtil.TRANSACTIONRECEIVED);

                TransactionModel transactionModel = new TransactionModel();
                transactionModel.setClientName(item.getClientName());
                transactionModel.setPayment(item.getPayment());
                transactionModel.setItemName(item.getItemName());
                transactionModel.setNetAmount(new BigDecimal(item.getNetAmount().replaceAll(",", "")));
                transactionModel.setVat(new BigDecimal(item.getVat().replaceAll(",", "")));
                transactionModel.setBranchLocation(item.getBranchLocation());
                transactionModel.setTimeStamp(DateUtil.toLocalDateTime(item.getTimeStamp()));
                ClientModel clientModel = clientRepository.findByClientName(item.getClientName());
                if(null != clientModel){
                    transactionModel.setClient(clientModel);
                }else{
                    log.info("Bad Record no Client available: "+item.toString());
                    statisticsHandlerUtil.getBadTransactionRecords().add(item.toString());
                    statisticsHandlerUtil.increment(StatisticsHandlerUtil.TRANSACTIONFAILED);
                    return null;
                }
                return transactionModel;
            }
        }catch (Exception e){
            log.error("Unhandaled Exception: "+e.toString());
        }
        return null;
    }
}
