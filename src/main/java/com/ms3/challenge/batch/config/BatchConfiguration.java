package com.ms3.challenge.batch.config;

import com.ms3.challenge.batch.business.Client;
import com.ms3.challenge.batch.business.Transaction;
import com.ms3.challenge.batch.listener.ClientItemReadListener;
import com.ms3.challenge.batch.listener.TransactionItemReadListener;
import com.ms3.challenge.batch.listener.ClientJobNotificationListener;
import com.ms3.challenge.batch.listener.TransactionJobNotificationListener;
import com.ms3.challenge.batch.processer.ClientItemProcessor;
import com.ms3.challenge.batch.processer.TransactionItemProcessor;
import com.ms3.challenge.batch.writer.ClientWriter;
import com.ms3.challenge.batch.writer.TransactionWriter;
import com.ms3.challenge.data.model.ClientModel;
import com.ms3.challenge.data.model.TransactionModel;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileParseException;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
@EnableBatchProcessing
class BatchConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public FlatFileItemReader<Client> clientReader() {
        return new FlatFileItemReaderBuilder<Client>()
                .name("clientItemReader")
                .resource(new ClassPathResource("CodingChallenge - Clients.csv"))
                .delimited()
                .names(new String[]{"clientName", "contactNum", "mailingAdd","memberSince","branch"})
                .fieldSetMapper(new BeanWrapperFieldSetMapper<Client>() {{
                    setTargetType(Client.class);
                }})
                .build();
    }

    @Bean
    public FlatFileItemReader<Transaction> transactionReader() {
        return new FlatFileItemReaderBuilder<Transaction>()
                .name("transactionItemReader")
                .resource(new ClassPathResource("CodingChallenge - Transactions.csv"))
                .delimited()
                .names(new String[]{"clientName", "payment", "itemName","netAmount","vat","branchLocation","timeStamp"})
                .fieldSetMapper(new BeanWrapperFieldSetMapper<Transaction>() {{
                    setTargetType(Transaction.class);
                }})
                .build();
    }

    @Bean
    public ClientItemProcessor clientProcessor() {
        return new ClientItemProcessor();
    }

    @Bean
    public TransactionItemProcessor transactionProcessor() {
        return new TransactionItemProcessor();
    }

    @Bean
    public ClientWriter clientWriter() {
        return new ClientWriter();
    }

    @Bean
    public TransactionWriter transactionWriter() {
        return new TransactionWriter();
    }

    // tag::jobstep[]
    @Bean
    public Job importClientsJob(ClientJobNotificationListener clientJobNotificationListener) {
        return jobBuilderFactory.get("importClientsJob")
                .incrementer(new RunIdIncrementer())
                .listener(clientJobNotificationListener)
                .flow(clientStep1(clientWriter()))
                .end()
                .build();
    }

    @Bean
    public Job importTransactionsJob(TransactionJobNotificationListener transactionJobNotificationListener){
        return jobBuilderFactory.get("importTransactionssJob")
                .incrementer(new RunIdIncrementer())
                .listener(transactionJobNotificationListener)
                .flow(transactionStep1(transactionWriter()))
                .end()
                .build();
    }

    @Bean
    public ClientItemReadListener clientListener(){
        return new ClientItemReadListener();
    }

    @Bean
    public TransactionItemReadListener transactionListener(){
        return new TransactionItemReadListener();
    }

    @Bean
    public Step transactionStep1(TransactionWriter writer){
        return stepBuilderFactory.get("step1")
                .<Transaction, TransactionModel> chunk(4)
                .reader(transactionReader()).listener(transactionListener())
                .processor(transactionProcessor())
                .writer(writer)
                .faultTolerant().skip(FlatFileParseException.class).skipPolicy(new ItemSkipPolicy()).build();
    }

    @Bean
    public Step clientStep1(ClientWriter clientWriter) {
        return stepBuilderFactory.get("step1")
                .<Client, ClientModel> chunk(4)
                .reader(clientReader()).listener(clientListener())
                .processor(clientProcessor())
                .writer(clientWriter)
                .faultTolerant().skip(FlatFileParseException.class).skipPolicy(new ItemSkipPolicy()).build();
    }

    class ItemSkipPolicy implements SkipPolicy {

        @Override
        public boolean shouldSkip(Throwable t, int skipCount) throws SkipLimitExceededException {
            return true;
        }
    }
}
