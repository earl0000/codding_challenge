package com.ms3.challenge.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author earl
 */
public class DateUtil {
    private static String dateFormat = "MMMM d, yyyy";

    private static String dataTimeFormat = "MMMM d, yyyy HH:mm";


    public static LocalDate toLocalDate(String date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
        return LocalDate.parse(date, formatter);
    }

    public static LocalDateTime toLocalDateTime(String date){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dataTimeFormat);
        return LocalDateTime.parse(date, formatter);
    }
}
