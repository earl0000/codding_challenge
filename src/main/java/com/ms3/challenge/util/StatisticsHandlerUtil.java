package com.ms3.challenge.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;

/**
 * @author earl
 */
@Component
public class StatisticsHandlerUtil {
    private static final Logger log = LoggerFactory.getLogger(StatisticsHandlerUtil.class);

    private static String CLIENTFILENAME = "bad-record_clients";
    private static String TRANSACTIONFILENAME = "bat-records_transactions";

    private static String CLIENTSTATISTICLOG = "logfile_client";
    private static String TRANSACTIONSTATISTICLOG = "logfile_transacation";

    public static String CLIENTSUCCESS = "clientsuccess";
    public static String CLIENTFAILED = "clientfailed";
    public static String CLIENTRECEIVED = "clientreceived";

    public static String TRANSACTIONSUCCESS = "transactionsuccess";
    public static String TRANSACTIONFAILED = "transactionfailed";
    public static String TRANSACTIONRECEIVED = "transactionreceived";

    @Value("${client.file.records.dir:./}")
    private String clientPath;

    @Value("${transaction.file.records.dir:./}")
    private String transactionPath;


    List<String> badClientRecords, badTransactionRecords;

    Predicate<Object> nonNull = Objects::nonNull;
    Predicate<Object> isNotEmptyList = list -> ((List)list).size() > 0;

    Map<String, Long> counters;

    public StatisticsHandlerUtil(){
        counters = new HashMap<String, Long>();
    }

    public List<String> getBadTransactionRecords() {
        if (null == badTransactionRecords){
            badTransactionRecords = new ArrayList<String>();
        }
        return badTransactionRecords;
    }

    public void setBadTransactionRecords(List<String> badTransactionRecords) {
        this.badTransactionRecords = badTransactionRecords;
    }

    public List<String> getBadClientRecords() {
        if (null == badClientRecords){
            badClientRecords = new ArrayList<String>();
        }

        return badClientRecords;
    }

    public void setBadClientRecords(List<String> badClientRecords) {
        this.badClientRecords = badClientRecords;
    }

    public void writeClientBadRecords(){
        writeBadRecords(clientPath+ File.separator+CLIENTFILENAME,badClientRecords);
    }

    public void writeTransactionBadRecords(){
        writeBadRecords(transactionPath+File.separator+TRANSACTIONFILENAME,badTransactionRecords);
    }

    public void writeBadRecords(String filepath, List<String> records)
    {
        if (nonNull.and(isNotEmptyList).test(records)){

            try (BufferedWriter bw = new BufferedWriter(new FileWriter(filepath+"_"+LocalDate.now().toString()+".csv"))) {


                records.forEach(s -> {
                    try {
                        bw.write(s+"\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });


            } catch (IOException e) {

                log.error("Unhandled Exception : "+e.toString());

            }
        }

    }

    public void increment(String counterName)
    {
        increment(counterName, 1);
    }

    public void increment(String counterName, int count)
    {
        Long i = counters.get(counterName);
        if (i == null)
        {
            i = new Long(0);
            counters.put(counterName, i);
        }
        i += count;
        counters.put(counterName, i);
    }

    public void writeStatistics(String logfile,String receive,String success, String failed){

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(logfile+"-"+ LocalDate.now().toString()+".txt"))) {


            bw.write("# of records received: "+(counters.get(receive)==null?0:counters.get(receive)));
            bw.write("\n# of records successful: "+(counters.get(success)==null?0:counters.get(success).toString()));
            bw.write("\n# of records failed: "+(counters.get(failed)==null?0:counters.get(failed).toString()));


        } catch (IOException e) {

            log.error("Unhandled Exception : "+e.toString());

        }
    }

    public void writeTransactionStatistics(){
        writeStatistics(clientPath+ File.separator+TRANSACTIONSTATISTICLOG,TRANSACTIONRECEIVED,TRANSACTIONSUCCESS,TRANSACTIONFAILED);
    }

    public void writeClientStatistics(){
        writeStatistics(clientPath+ File.separator+CLIENTSTATISTICLOG,CLIENTRECEIVED,CLIENTSUCCESS,CLIENTFAILED);
    }
}
