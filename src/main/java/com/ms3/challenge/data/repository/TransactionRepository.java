package com.ms3.challenge.data.repository;

import com.ms3.challenge.data.model.TransactionModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author earl
 */
@Repository
public interface TransactionRepository extends CrudRepository<TransactionModel, Long> {
}
