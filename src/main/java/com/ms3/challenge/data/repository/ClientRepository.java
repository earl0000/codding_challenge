package com.ms3.challenge.data.repository;

import com.ms3.challenge.data.model.ClientModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author earl
 */
@Repository
public interface ClientRepository extends CrudRepository<ClientModel, Long> {
    public ClientModel findByClientName(String clientName);
}
