package com.ms3.challenge.data.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

/**
 * @author earl
 */
@Entity
@Table(name = "client")
public class ClientModel {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long clientid;
    private String clientName;
    private String contactNum;
    private String mailingAdd;
    private LocalDate memberSince;
    private String branch;

    @OneToMany(mappedBy="client")
    private Set<TransactionModel> transactions;

    public Set<TransactionModel> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<TransactionModel> transactions) {
        this.transactions = transactions;
    }

    public Long getClientid() {
        return clientid;
    }

    public void setClientid(Long clientid) {
        this.clientid = clientid;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getContactNum() {
        return contactNum;
    }

    public void setContactNum(String contactNum) {
        this.contactNum = contactNum;
    }

    public String getMailingAdd() {
        return mailingAdd;
    }

    public void setMailingAdd(String mailingAdd) {
        this.mailingAdd = mailingAdd;
    }

    public LocalDate getMemberSince() {
        return memberSince;
    }

    public void setMemberSince(LocalDate memberSince) {
        this.memberSince = memberSince;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }
}
