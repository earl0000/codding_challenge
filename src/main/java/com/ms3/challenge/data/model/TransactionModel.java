package com.ms3.challenge.data.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author earl
 */
@Entity
@Table(name = "transaction")
public class TransactionModel {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long transactionid;
    @ManyToOne
    @JoinColumn(name="clientid", nullable=false)
    private ClientModel client;

    private String clientName;
    private String payment;
    private String itemName;
    private BigDecimal netAmount;
    private BigDecimal vat;
    private String branchLocation;
    private LocalDateTime timeStamp;


    public Long getTransactionid() {
        return transactionid;
    }

    public void setTransactionid(Long transactionid) {
        this.transactionid = transactionid;
    }

    public ClientModel getClient() {
        return client;
    }

    public void setClient(ClientModel client) {
        this.client = client;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getBranchLocation() {
        return branchLocation;
    }

    public void setBranchLocation(String branchLocation) {
        this.branchLocation = branchLocation;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    public BigDecimal getNetAmount()
    {
        return netAmount;
    }

    public void setNetAmount(BigDecimal netAmount)
    {
        this.netAmount = netAmount;
    }

    public BigDecimal getVat()
    {
        return vat;
    }

    public void setVat(BigDecimal vat)
    {
        this.vat = vat;
    }
}
