import com.ms3.challenge.Application;
import com.ms3.challenge.data.model.ClientModel;
import com.ms3.challenge.data.repository.ClientRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * @author earl
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@DataJpaTest
public class RepositoryTest {

    private static final Logger logger = LogManager.getLogger(RepositoryTest.class.getName());

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private TestEntityManager entityManager;

    private ClientModel client;

    @Before
    public void setup(){
        client = new ClientModel();
        client.setClientName("test");
        client.setBranch("here");
        client.setContactNum("test");
        client.setMailingAdd("test");
        client.setMemberSince(LocalDate.now());
    }


    @Test
    public void clientSaveTest(){

        entityManager.persist(client);
        entityManager.flush();

        ClientModel found = clientRepository.findByClientName(client.getClientName());
        assertThat(found.getClientName())
                .isEqualTo(client.getClientName());

        logger.info("Database size: "+clientRepository.count());

    }
}
